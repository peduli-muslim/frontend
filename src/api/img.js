import api from "."

const imgApi = {
  upload: (data, onUploadProgress, signal) => {
    return api.post(`/upload/image`, data, { "Content-Type": "multipart/form-data", onUploadProgress, signal })
  }
}

export default imgApi