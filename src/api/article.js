import api from "."

const articleApi = {
  postArticle: (data) => {
    return api.post(`/article`, data)
  },
  getAll: () => {
    return api.get(`/article`)
  },
  addImages: (data, onUploadProgress) => {
    return api.post(`/article/image/add`, data, { "Content-Type": "multipart/form-data", onUploadProgress: onUploadProgress })
  },
  getOne: (uuid) => {
    return api.get(`/article/${uuid}`)
  },
  update: (data, uuid) => {
    return api.put(`/article/${uuid}`, data)
  }
}

export default articleApi