import api from ".";

const admin = {
    login: (data) => {
        return api.post("/admin/login", data)
    },
    logout: () => {
        return api.get("/admin/logout")
    },
    updatePass: (data) => {
        return api.put("/admin/password", data)
    },
    getAdmin: () => {
        return api.get("admin/profile")
    },
    updateAdmin: (data) => {
        return api.put("admin/profile", data)
    },
    addAdmin: (data) => {
        return api.post("/admin/register", data)
    },
    getAll: () => {
        return api.get("/admin")
    }
}

export default admin
