import api from "."

const userApi = {
    getUser: () => {
        return api.get(`/user/profile`)
    },
    updateUser: (data) => {
        return api.put(`/user`, data)
    },
}

export default userApi