import api from "."

const programApi = {
    getProgram: () => {
        return api.get("/program")
    },
    getbyUuid: (uuid) => {
        return api.get(`/program/${uuid}`)
    },
    updateProgram: (data, uuid) => {
        return api.put(`/program/${uuid}`, data)
    },
    addProgram: (data) => {
        return api.post("/program", data)
    }, 
    addImage: (data, onUploadProgress) => {
        return api.post(`/program/image/add`, data, { "Content-Type": "multipart/form-data", onUploadProgress: onUploadProgress })
    },
    toggleStatus: (uuid) => {
        return api.get(`/program/status/${uuid}`)
    }
}

export default programApi