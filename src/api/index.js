import { ClientID, ClientSecret } from "@/google";
import store from "@/store";
import axios from "axios";

const baseURL = "http://192.168.1.108:8000"
// baseURL: "http://192.168.43.22:8000"
// baseURL: "http://192.168.88.246:8000"

const api = axios.create({
  baseURL: baseURL,
})

const googleRefresh = (data) => {
  return axios.post("https://www.googleapis.com/oauth2/v4/token", data)
}

const localRefresh = () => axios.get(`${baseURL}/auth/refresh`, { headers: { Authorization: `Bearer ${store.getters.getRefreshToken}` } })

api.interceptors.request.use((req) => {
  if (!req.url.includes("/login") || !req.url.includes("/register")) {
    req.headers = { Authorization: `Bearer ${store.getters.getAccessToken}` }
  }
  return req
})

api.interceptors.response.use(
  async (res) => {
    return res
  }, async (err) => {
    const originalRequest = err.config;
    if (err.response.status === 401 && !originalRequest._retry && store.getters.getUser) {
      originalRequest._retry = true;
      try {
        if (store.getters.getLoginWith == "local") {
          let res = await localRefresh()
          if (res.status == 200) {
            await store.dispatch("setAccessToken", res.data.data.access_token);
            originalRequest.headers.Authorization = `Bearer ${res.data.data.access_token}`
            return api(originalRequest);
          }
        } else {
          let res = await googleRefresh({
            client_id: ClientID,
            client_secret: ClientSecret,
            refresh_token: store.getters.getRefreshToken,
            grant_type: "refresh_token",
          })
          if (res.status == 200) {
            await store.dispatch("setAccessToken", res.data.access_token);
            originalRequest.headers.Authorization = `Bearer ${res.data.access_token}`
            return api(originalRequest);
          }
        }
      } catch (error) {
        return Promise.reject(err);
      }
    } else return Promise.reject(err);

  }
)

export default api