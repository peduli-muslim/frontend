import api from "."

const donasiApi = {
  postDonasi: (programID, data) => {
    return api.post(`/donatur/${programID}`, data)
    // return axios.post(`/donatur/${programID}`, data)
  },
  getbyProgram: (programID) => {
    return api.get(`/donatur/${programID}`)
  }
}

export default donasiApi