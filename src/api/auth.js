import api from "."

const authApi = {
    getLocalAuth: () => {
        return api.get(`/auth`)
    },
    updateUsername: (data) => {
        return api.put(`/auth/username`, data)
    },
    updatePassword: (data) => {
        return api.put(`/auth/password`, data)
    },
    localLogin: (data) => {
        return api.post(`/auth/login`, data)
    },
    localLogout: () => {
        return api.get(`/auth/logout`)
    },
    googleLogin: () => `${api.defaults.baseURL}/google/login`,
    register: (data) => {
        return api.post(`auth/register`, data)
    },
    verifygoogle: (data) => {
        return api.post(`/google/verify`, data)
    },
    tokengoogle: (data) => {
        return api.post(`/google/token`, data)
    }
}

export default authApi