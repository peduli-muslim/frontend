import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import '../src/assets/style/index.css'
// import VueSocialSharing from 'vue-social-sharing'
import GAuth from "vue3-google-oauth2"
import { ClientID } from './google'

const app = createApp(App)
app.use(store)
app.use(router)
// app.use(VueSocialSharing)
app.use(
  GAuth, {
  clientId: ClientID,
  scope: 'email',
  prompt: 'consent',
  fetch_basic_profile: true,
})
app.provide("gAuth", app.config.globalProperties.$gAuth)
app.mount('#app')