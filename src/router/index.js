import { createRouter, createWebHistory } from 'vue-router'
import store from "@/store";
// import { useStore } from 'vuex';
import adminRouter from './admin'
import publicRouter from './public'
// import Login from "../views/Login"
// import LoginAdmin from "../views/admin/LoginAdmin"
import Login from "../views/public/Login"
import LoginAdmin from "../views/admin/LoginAdmin"
import NotFound from "../views/NotFound"
import TestView from "../views/Test"

// const store = useStore()

const routes = [
  publicRouter,
  adminRouter,
  {
    path: '/login',
    name: 'Login',
    component: Login,
    meta: {
      public: true,
    },
  },
  {
    path: '/admin/login',
    name: 'Login Admin',
    component: LoginAdmin,
  },
  {
    path: "/test",
    name: "Test",
    component: TestView,
  },
  {
    path: "/:patchMatch(.*)",
    name: "Not Found",
    component: NotFound,
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.user)) {
    if (store.getters.getUser != null) {
      next()
    } else {
      next("/login")
    }
  } else if (to.matched.some(record => record.meta.admin)) {
    if (store.getters.getAdmin != null) {
      next()
    } else {
      next("/admin/login")
    }
  } else {
    // if (store.getters.getUser != null) {
    //   next("/")
    // } else if (store.getters.getAdmin != null) {
    //   next("/admin")
    // } else {
    //   next()
    // }
    next()
  }
})

router.afterEach((to) => {
  document.title = to.name
})

export default router