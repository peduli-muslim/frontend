import HomeAdmin from "../views/admin/HomeAdmin"
import AddProgram from "../views/admin/AddProgram"
import AddArticle from "../views/admin/AddArticle"
import AddAdmin from "../views/admin/AddAdmin"
import UpdateArticle from "../views/admin/UpdateArticle"
import Article from "../views/admin/Article"
import DaftarAdmin from "../views/admin/DaftarAdmin"
import UpdateProgram from "../views/admin/UpdateProgram"
import AdminLayout from "../layout/main/AdminLayout"

const adminRouter = {
  path: "/admin",
  redirect: "/admin/home",
  name: "Admin",
  component: AdminLayout,
  children: [
    {
      path: "/admin/home",
      name: "Home Admin",
      component: HomeAdmin,
      meta: {
        admin: true,
      },
    },
    {
      path: "/admin/tambah-program",
      name: "AddProgram",
      component: AddProgram,
      meta: {
        admin: true,
      },
    },
    {
      path: "/admin/tambah-artikel",
      name: "AddArticle",
      component: AddArticle,
      meta: {
        admin: true,
      },
    },
    {
      path: "/admin/update-artikel/:uuid",
      name: "UpdateArticle",
      component: UpdateArticle,
      meta: {
        admin: true,
      },
    },
    {
      path: "/admin/artikel",
      name: "Article",
      component: Article,
      meta: {
        admin: true,
      },
    },
    {
      path: "/admin/update-program/:uuid",
      name: "UpdateProgram",
      component: UpdateProgram,
      meta: {
        admin: true,
      },
    },
    {
      path: "/admin/tambah-admin",
      name: "AddAdmin",
      component: AddAdmin,
      meta: {
        admin: true,
      },
    },
    {
      path: "/admin/daftar-admin",
      name: "DaftarAdmin",
      component: DaftarAdmin,
      meta: {
        admin: true,
      },
    },
  ]
}

export default adminRouter
