import Program from '../views/public/Program'
import Detail from '../views/public/Detail'
import Donasi from '../views/public/Donasi'
import Register from '../views/public/Register'
import PublicLayout from "../layout/main/PublicLayout"
import Home from "../views/public/Home"
import Artikel from "../views/public/Artikel"

const publicRouter = {
  path: "/",
  redirect: "/home",
  name: "Public",
  component: PublicLayout,
  children: [
    {
      path: '/home',
      name: 'Home',
      component: Home,
      meta: {
        public: true,
      },
    },
    {
      path: '/article/:uuid',
      name: 'Artikel',
      component: Artikel,
      meta: {
        public: true,
      },
    },
    {
      path: '/program',
      name: 'Program',
      component: Program,
      meta: {
        public: true,
      },
    },
    {
      path: '/detail/:uuid',
      name: 'Detail',
      component: Detail,
      meta: {
        public: true,
      },
    },
    {
      path: '/donasi/:uuid',
      name: 'Donasi',
      component: Donasi,
      meta: {
        user: true,
      },
    },
    {
      path: '/register',
      name: 'Register',
      component: Register,
      meta: {
        public: true,
      },
    },
    {
      path: '/about',
      name: 'About',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import( /* webpackChunkName: "about" */ '../views/public/AboutView.vue')
    },
  ]
}

export default publicRouter