import { createStore } from 'vuex'
import VuexPersistence from 'vuex-persist'

const vuexLocalStorage = new VuexPersistence({
  key: "store",
  storage: window.localStorage,
})

export default createStore({
  plugins: [vuexLocalStorage.plugin],
  state: {
    accessToken: "",
    refreshToken: "",
    user: null,
    admin: null,
    loginWith: "",
  },
  getters: {
    getAccessToken: (state) => state.accessToken,
    getRefreshToken: (state) => state.refreshToken,
    getUser: (state) => state.user,
    getAdmin: (state) => state.admin,
    getLoginWith: (state) => state.loginWith
  },
  mutations: {
    setAccessToken: (state, data) => {
      state.accessToken = data
    },
    setRefreshToken: (state, data) => {
      state.refreshToken = data
    },
    setUser: (state, data) => {
      state.user = data
    },
    setAdmin: (state, data) => {
      state.admin = data
    },
    setLoginWith: (state, data) => {
      state.loginWith = data
    }
  },
  actions: {
    setAccessToken: (context, data) => {
      context.commit("setAccessToken", data)
    },
    setRefreshToken: (context, data) => {
      context.commit("setRefreshToken", data)
    },
    login: (context, data) => {
      context.commit("setAccessToken", data.access_token)
      context.commit("setRefreshToken", data.refresh_token)
      if (data.user) context.commit("setUser", data.user)
      if (data.admin) context.commit("setAdmin", data.admin)
    },
    logout: (context) => {
      console.log("Logout")
      context.commit("setAccessToken", null)
      context.commit("setRefreshToken", null)
      context.commit("setLoginWith", null)
      context.commit("setUser", null)
      context.commit("setAdmin", null)
    },
    setUser: (context, data) => {
      context.commit("setUser", data)
    },
    setAdmin: (context, data) => {
      context.commit("setAdmin", data)
    },
    setLoginWith: (context, data) => {
      context.commit("setLoginWith", data)
    }
  },
})