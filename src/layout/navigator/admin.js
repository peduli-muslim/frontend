const adminNav = [
  {
    path: "/admin/home",
    title: "Home"
  },
  {
    path: "/admin/daftar-admin",
    title: "Daftar Admin"
  },
  {
    path: "/admin/tambah-admin",
    title: "Tambah Admin"
  },
  {
    path: "/admin/tambah-program",
    title: "Tambah Program",
  },
  {
    path: "/admin/tambah-artikel",
    title: "Tambah Artikel",
  },
  {
    path: "/admin/artikel",
    title: "Artikel",
  },
] 

export default adminNav