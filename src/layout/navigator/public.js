const publicNav = [
  {
    title: "Home",
    path: "/home"
  },
  {
    title: "Program",
    path: "/program"
  },
  {
    title: "About",
    path: "/about"
  }, {
    title: "Admin",
    path: "/admin/login"
  },
]

export default publicNav