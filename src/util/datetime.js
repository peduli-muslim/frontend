import moment from "moment"

const dateID = (date) => {
    return moment(date).format("DD-MM-YYYY")
}

export {dateID}