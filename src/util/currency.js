const toRupiah = (nilai) => {
    let stringNilai = nilai.toString();
    let sementara = stringNilai
        .substring(stringNilai.length % 3)
        .match(/.{1,3}/g)
        ?.join(".");
    let sisa = stringNilai.substring(0, stringNilai.length % 3);
    if (!sementara) {
        return sisa;
    } else if (sisa != "") {
        return sisa + "." + sementara;
    } else {
        return sementara;
    }
}

export { toRupiah }